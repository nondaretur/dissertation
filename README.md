# Scalice Dissertation ReadMe

### JPSScript build 

The document can be compiled by running JPSScript.

```bash
$ ./JPSScript.sh
```

### TexMaker Settings

This document was written using Vim on Debian Linux. Here are the settings for compiling on [Texmaker](http://www.xm1math.net/texmaker/) if you prefer working in a GUI. 

Latexmk

```bash
latexmk -auxdir=Metafiles -outdir=Metafiles -e "$pdflatex=q/pdflatex -synctex=1 -interaction=nonstopmode/" -pdf %.tex
```

Biblatex compile option 

```bash
biber --input-directory=Metafiles --output-directory=Metafiles %
```

Makeindex compile option

```bash
./indices.sh
```

To compile glossaries I created the custom user command

```bash
pdflatex -synctex=1 -interaction=nonstopmode %.tex|makeglossaries -d Metafiles %|pdflatex -synctex=1 -interaction=nonstopmode %.tex|pdflatex -synctex=1 -interaction=nonstopmode %.tex
```

Finally I created a symbolic link from the build pdf to the root directory

```bash
ln -s Metafiles/Dissertation.pdf Dissertation.pdf
```
