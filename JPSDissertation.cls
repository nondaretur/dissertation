%%
%% Scalice Dissertation LaTeX Class
%%
%% Inspired in part by Eivind Uggedal's Thesis [github.com/jrk/uggedal-thesis/]
%% And in keeping with the requirements of UC Berkeley Dissertation style guidelines [ctan.org/pkg/ucbthesis]
%%

\ProvidesClass{JPSDissertation}
\NeedsTeXFormat{LaTeX2e}

\RequirePackage[l2tabu, orthodox]{nag}

% Based on the ucbthesis class
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{ucbthesis}}
\ProcessOptions
\LoadClass{ucbthesis}

%%
%% Variables
%%

% Internal variables
\newcommand{\@subtitle}{}
\newcommand{\@scm}{}

% Outside variables
\newcommand{\subtitle}[1]{\renewcommand{\@subtitle}{#1}}
\newcommand{\scm}[1]{\renewcommand{\@scm}{#1}}

%%
%% Fonts
%% 

\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[osf, p]{libertine}
\usepackage{textcomp}
\usepackage[super]{nth}
\usepackage{ellipsis}
\usepackage{hologo}

\newcommand{\lowercaps}[1]{\textsc{\MakeLowercase{#1}}}
\newcommand{\uppercaps}[1]{\textsc{\MakeUppercase{#1}}}

% Creates an fourier ornament. Args: fontsize, lineheight, char
\RequirePackage{fourier-orns}
\newcommand*{\fourierOrnament}[3]{{%
  \fontencoding{U}\fontfamily{futs}\fontsize{#1}{#2}\selectfont\char#3}}

%%
%% Page layout
%%

\setulmarginsandblock{3cm}{3.5cm}{*}
\setlrmarginsandblock{4cm}{4cm}{*}
\checkandfixthelayout

% No orphans
\clubpenalty = 500
\widowpenalty = 1000

% Add or remove lines from facing pages
\RequirePackage{addlines}

%%
%% Line breaking
%%

\usepackage[final=true]{microtype}
\microtypecontext{spacing=french}
\RequirePackage{ragged2e}

%%
%% Colors
%%

\RequirePackage{xcolor}

\definecolor{red}{HTML}{AD1737}
\definecolor{brown}{HTML}{990000}
\definecolor{bar}{HTML}{8D9965}

\definecolor{chapnum}{gray}{0.6}
\definecolor{largeornament}{gray}{0.85}
\definecolor{smallornament}{gray}{0.65}

\colorlet{title}{red}
\colorlet{urlcolor}{brown}
\colorlet{tocchapnum}{brown}

%%
%% Graphics
%%

\usepackage{graphicx}
\graphicspath{{./Images/}}

\usepackage{wrapfig}

\usepackage{sparklines}

%%
%% Division styles
%%

% Part style

\newcommand{\partimage}[2][]{\gdef\@partimage{\includegraphics[#1]{#2}}} % part image macro
\renewcommand{\printparttitle}[1]{\vfil\vfil\vfil\vfil\vfil\vfil\vfil\vfil \parttitlefont #1\vfil\@partimage\vfil\vfil\vfil\vfil\vfil\vfil\vfil\vfil}
\makeatother

\partimage[width=0.4\textwidth]{line.eps}

% Chapter style
\chapterstyle{madsen}

% Appendices
\usepackage{appendix}

%%
%% Headers and Footers
%%

\pagestyle{companion}

% Empty part page style
\aliaspagestyle{part}{empty}

%%
%% Epigraph environment settings 
%%

\usepackage{epigraph}
	\epigraphfontsize{\small\itshape}
	\setlength\epigraphwidth{12.5cm}
	\setlength\epigraphrule{0pt}

%%
%% Lists
%%

% Table of Contents

\maxtocdepth{section}

\usepackage{tocloft}
\usepackage[normalem]{ulem} % Underlining part header
	\useunder{\uline}{\ul}{}

\renewcommand*{\cftdotsep}{1} % page number width in toc expansion to fit larger numbers
\setpnumwidth{3em}
\setrmarg{3.5em}

\renewcommand{\insertchapterspace}{} % no chapter spacing in LOF and LOT

\renewcommand{\cftpartafterpnum}{\vspace{-5pt}}
\cftsetindents{chapter}{1.5em}{1.5em}
\cftsetindents{section}{3.8em}{3.2em}
\renewcommand{\cftpartfont}{\LARGE\sffamily} 
\setlength{\cftbeforechapterskip}{0.25em}

\renewcommand\partnumberlinebox[2]{#2\hspace{1em}} % adjust space between part number and part title

\renewcommand{\cftchapterpresnum}{\color{tocchapnum}}

\newcommand{\nocontentsline}[3]{} % Exclude section from TOC
\newcommand{\tocless}[2]{\bgroup\let\addcontentsline=\nocontentsline#1{#2}\egroup}

% Enumerated lists
\usepackage{enumitem}


%%
%% Captions
%%

\usepackage{caption}
\DeclareCaptionFormat{myformat}{#1#2#3\hrule}

\captionsetup[figure]
	{format=myformat,
	indention=.5cm,
	font=footnotesize,
	justification=raggedright,
	singlelinecheck=false
	}

\captionsetup[map]
	{indention=.5cm,
	font=footnotesize,
	justification=raggedright,
	singlelinecheck=false
	}

\usepackage{float}
	\floatstyle{plaintop}
	\newsubfloat{figure}

%% % Define maps

%% \newfloat{map}{htbp}{map}[chapter]\floatname{map}{Map}
%% \makeatletter
%%	\renewcommand*{\float@listhead}[1]{%
%%  		\@ifundefined{chapter}{%
%%    	\section*{#1}%
%%    	\addcontentsline{toc}{section}{#1}%
%%  		}{%
%%    	\chapter*{#1}%
%%    	\addcontentsline{toc}{chapter}{#1}%
%%  		}%
%%  		\@mkboth{\MakeUppercase{#1}}{\MakeUppercase{#1}}%
%%	}
%%\makeatother

%%\newcommand{\listofmaps}{\listof{map}{List of Maps}}

%%
%% Tables
%%

\usepackage{booktabs}
\usepackage{longtable}
\usepackage{multirow}

%%
%% Environments
%%

\newlength{\foreminusspine}\setlength{\foreminusspine}{\foremargin}
\addtolength{\foreminusspine}{-\spinemargin}

% Centered environment
\newenvironment{centered}{%
  \begin{adjustwidth*}{0em}{-\foreminusspine}
    \centering
}{%
  \end{adjustwidth*}
}


%%
%% Hyperlinks
%%

\usepackage{url}

\makeatletter
\let\UrlSpecialsOld\UrlSpecials
\def\UrlSpecials{\UrlSpecialsOld\do\/{\Url@slash}\do\_{\Url@underscore}}%
\def\Url@slash{\@ifnextchar/{\kern-.11em\mathchar47\kern-.2em}%
    {\kern-.0em\mathchar47\kern-.08em\penalty\UrlBigBreakPenalty}}
\def\Url@underscore{\nfss@text{\leavevmode \kern.06em\vbox{\hrule\@width.3em}}}
\makeatother

% break also on hyphens inside the \url command
\def\UrlBreaks{\do\.\do\@\do\\\do\/\do\!\do\_\do\|\do\;\do\>\do\]%
  \do\)\do\,\do\?\do\'\do+\do\=\do\#\do-} % \do- is new!

\urlstyle{sf}

\usepackage[hidelinks]{hyperref}

\usepackage{cleveref}

\crefname{section}{\S}{\S\S} % Section symbol
\Crefname{section}{\S}{\S\S}

\crefformat{section}{\S#2#1#3} % No space after Section symbol


%%
%% Quotations and citations
%%

% Language settings

\usepackage[american, headfoot=american]{babel}
\usepackage{./Data/jpshyphen}

% Bibliography settings

\usepackage
	[
	notes,
	backend=biber,
	isbn=false,
	sorting=nymdt,
	annotation,
	loccittracker=context
	]
	{biblatex-chicago}

% Subsubbibliography definition
\defbibheading{subsubbibliography}[\refname]{\subsection*{#1}}

% DD MM YYYY date format
\DefineBibliographyExtras{american}{%
  \protected\def\mkbibdatelong#1#2#3{%
    \iffieldundef{#3}
     {}
     {\stripzeros{\thefield{#3}}%
    \iffieldundef{#2}{}{\nobreakspace}}%
    \iffieldundef{#2}
     {}
     {\mkbibmonth{\thefield{#2}}%
    \iffieldundef{#1}{}{\space}}%
    \iffieldbibstring{#1}{\bibstring{\thefield{#1}}}				
     {\stripzeros{\thefield{#1}}}}%
  }

\makeatletter
      \newrobustcmd*{\parentexttrack}[1]{%
  	  \begingroup
 	  \blx@blxinit
      \blx@setsfcodes
  	  \blx@bibopenparen#1\blx@bibcloseparen
      \endgroup}

      \AtEveryCite{%
  	  \let\parentext=\parentexttrack}

\makeatother

% Bibliography sort scheme
\DeclareSortingScheme{nymdt}{
   \sort{
     \field{presort}
  	  }
  	\sort[final]{
     \field{sortkey}
  	  }
  	\sort{
    	\name{sortname}
    	\name{author}
    	\name{editor}
    	\name{translator}
	    \field{sorttitle}
    	\field{title}
  		}
  	\sort{
    	\field{sortyear}
    	\field{year}
  		}
  	\sort{
    	\field[padside=left,padwidth=2,padchar=0]{month}
    	\literal{00}
  		}
  	\sort{
    	\field[padside=left,padwidth=2,padchar=0]{day}
    	\literal{00}
  		}
  	\sort{
    	\field{sorttitle}
  		}
 	\sort{
    	\field[padside=left,padwidth=4,padchar=0]{volume}
    	\literal{0000}
  		}
	}

% Alternate footnote characters

\makeatletter
   \def\@xfootnote[#1]{%
  	\protected@xdef\@thefnmark{#1}%
  	\@footnotemark\@footnotetext}
\makeatother

% Quotes

\usepackage[maxlevel=3]{csquotes}

% Acronyms

\usepackage[smallcaps,acronym,noredefwarn,nonumberlist,nopostdot,nomain]{glossaries}
\usepackage{glossary-longragged}

\setacronymstyle{long-short}
\setglossarysection{subsection}
\loadglsentries{./Data/acronyms}
\glsdisablehyper
\makeglossaries

\renewcommand*{\acronymfont}[1]{\textls[80]{\textsc{#1}}}

%%
%% Indices
%%

\usepackage[xindy]{imakeidx}

\usepackage[itemlayout=singlepar,indentunit=1em,columnsep=1.25em,font=small]{idxlayout}

\makeindex[program=xindy,name=name,title=Name Index]
\makeindex[program=xindy,name=subject,title=Subject Index]
\makeindex[name=texts,title=Texts Index]

%%
%% Titlepage
%%

\RequirePackage{pdfpages}
\renewcommand{\maketitle}{%
  %%\includepdf[pages={1,{}}]{uiofrontpage}

  \begin{titlingpage}
    \begin{centered}
        \null\vspace{8pc}

        {\Huge\color{title}\uppercaps{Crisis}}
  	{\Huge\textit{{of}}} \\
        \vspace{2pc}
        {\Huge\color{title}\uppercaps{Revolutionary Leadership}} \\

        \vspace{4pc}
        {\Large\aldineleft} \\
        \vspace{4pc}

        {\Large\textit{{
          Martial Law and the Communist Parties of the Philippines, 1959--1974}}} \\

        \vspace{3pc}

      	{\large Joseph Scalice} \\

        \vspace{2pc}

        {\large Summer 2017} \\

        \vspace{2pc}

        \textit{
        A dissertation submitted in partial satisfaction of the requirements \\
        for the degree of Doctor of Philosophy \\
        \vspace{1pc}
        in \\
        \vspace{1pc}
        South and Southeast Asian Studies \\
        in the Graduate Division of the \\
        University of California, Berkeley \\
        }

        \vspace{1pc}

    	\textit{
       		\begin{minipage}[t]{0.4\textwidth}
    			\begin{flushleft}
        			\textup{Committee in Charge:}
    			\end{flushleft}
		\end{minipage}%
		%
		\begin{minipage}[t]{0.6\textwidth}
    			\begin{flushleft}
        			Associate Professor Jeffrey Hadler, Chair \\
        			Professor Peter Zinoman \\
				Professor Patricio Abinales 
    			\end{flushleft}
		\end{minipage}%
	}
        \vfill

      %%  \@scm
    \end{centered}
    \clearpage
  \end{titlingpage}
}
