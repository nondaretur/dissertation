Bump images a bit into margin. Side caption images. Something like that.

Need to figure out custom short title with Biblatex. Not dropping date. Dropping custom part of title.

Create small toc

It seems according to that I must put the appendices after the bibliography.

%%[http://grad.berkeley.edu/academic-progress/dissertation/#formatting-your-manuscript] 

Check on how the abstract should be numbered. Does the numbering begin on the abstract page? or does it begin with the prior copyright, etc?

Fix comma after question mark / exclamation point in footnotes. [Babel fixed bibliography for question marks but not !]
	The probelm isn't question mark versus exclamation point, but that babel fixed article type but not misc. 

Fix overfulls and badboxes. use [draft] option with {documentclass}

Brackets in implied titles should not be in italics.

Index

Description list


